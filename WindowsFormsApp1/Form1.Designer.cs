﻿
namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_display1 = new System.Windows.Forms.Label();
            this.btn_display1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_display1
            // 
            this.lbl_display1.Location = new System.Drawing.Point(119, 111);
            this.lbl_display1.Name = "lbl_display1";
            this.lbl_display1.Size = new System.Drawing.Size(225, 42);
            this.lbl_display1.TabIndex = 0;
            this.lbl_display1.Text = "lbl_display1";
            // 
            // btn_display1
            // 
            this.btn_display1.Location = new System.Drawing.Point(122, 274);
            this.btn_display1.Name = "btn_display1";
            this.btn_display1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_display1.Size = new System.Drawing.Size(103, 64);
            this.btn_display1.TabIndex = 1;
            this.btn_display1.Text = "Display";
            this.btn_display1.UseVisualStyleBackColor = true;
            this.btn_display1.Click += new System.EventHandler(this.btn_display1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(863, 507);
            this.Controls.Add(this.btn_display1);
            this.Controls.Add(this.lbl_display1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_display1;
        private System.Windows.Forms.Button btn_display1;
    }
}

